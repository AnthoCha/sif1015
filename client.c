#include "clientMessage.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <ncurses.h>

#define BORDER_X 2
#define BORDER_Y 5
#define TRANS_Y 2
#define TRANS_HEIGHT 10
#define RECEPT_Y TRANS_HEIGHT + TRANS_Y + BORDER_X
#define RECEPT_HEIGHT 20

#define CURS_X 5
#define CURS_Y 7

void readTerminal(WINDOW* transmission, struct clientMessage* clientMessage, struct sockaddr* serverAddr);

struct readClientMessageParams {
    struct clientMessage* clientMessage;
    WINDOW* reception;
    WINDOW* transmission;
    int attr;
};

void readClientMessageWithParams(struct readClientMessageParams* params);

int main(int argc, char* argv[]) {
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);

    if (argc < 2) {
        printf("Veuillez entrer l'addresse du serveur comme argument.");
        return EXIT_FAILURE;
    }

    if (inet_pton(AF_INET, argv[1], &serverAddr.sin_addr) <= 0) {
        perror("Addresse invalide.");
        return EXIT_FAILURE;
    }

    initscr();
    if (!has_colors()) {
        endwin();
        perror("Erreur: le terminal ne supporte pas la couleur.");
        return 1;
    }

    if (start_color() != OK) {
        endwin();
        perror("Erreur lors de l'activation des couleurs.");
        return 1;
    }

    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);

    WINDOW* transmission, * reception;
    int row, col;

    getmaxyx(stdscr, row, col);
    transmission = newwin(TRANS_HEIGHT, col - BORDER_Y, TRANS_Y, BORDER_X);
    box(transmission, '|', '-');
    mvwaddstr(transmission, 1, 1, "Transmission");
    mvwaddstr(transmission, 5, 5, "Entrez une ligne de commande:");
    wrefresh(transmission);

    reception = newwin(RECEPT_HEIGHT, col - BORDER_Y, RECEPT_Y, BORDER_X);
    box(reception, '|', '-');
    mvwaddstr(reception, 1, 1, "Reception");
    wrefresh(reception);

    wmove(transmission, CURS_Y, CURS_X);
    wrefresh(transmission);

    pthread_t tid;

    char quit = 0;

    do {
        struct clientMessage* clientMessage = malloc(sizeof(struct clientMessage));
        clientMessage->clientFd = socket(AF_INET, SOCK_STREAM, 0);

        if (clientMessage->clientFd == -1) {
            perror("Erreur lors de la création du socket.");
            free(clientMessage);
            return EXIT_FAILURE;
        }

        readTerminal(transmission, clientMessage, &serverAddr);

        struct readClientMessageParams* params = malloc(sizeof(struct readClientMessageParams));
        params->clientMessage = clientMessage;
        params->reception = reception;
        params->transmission = transmission;

        if (clientMessage->message[0] == 'L' || clientMessage->message[0] == 'l') {
            params->attr = COLOR_PAIR(2);
        } else {
            params->attr = COLOR_PAIR(1);
        }

        if (clientMessage->message[0] == 'Q' || clientMessage->message[0] == 'q') {
            quit = 1;
        }


        if (clientMessage->message[0] != 0 && pthread_create(&tid, NULL, readClientMessageWithParams, params) != 0) {
            perror("Erreur lors de la création d'un thread de lecture.");
            return 1;
        }
    } while (quit == 0);

    pthread_join(tid, NULL);

    endwin();

    return 0;
}

void readClientMessageWithParams(struct readClientMessageParams* params) {
    wclear(params->reception);
    wattrset(params->reception, params->attr);
    box(params->reception, '|', '-');
    mvwaddstr(params->reception, 1, 1, "Reception");
    wrefresh(params->reception);

    int nbLigne = 0;

    while (strcmp(params->clientMessage->message, MSG_ACK)) {
        if (read(params->clientMessage->clientFd, params->clientMessage->message, CLIENT_MESSAGE_BUFFER) >= 0 &&
            strcmp(params->clientMessage->message, MSG_ACK)) {
            mvwaddstr(params->reception, 2 + nbLigne, 2, params->clientMessage->message);
            wrefresh(params->reception);
            nbLigne++;
        }
    }

    wmove(params->transmission, CURS_Y, CURS_X);
    wrefresh(params->transmission);

    close(params->clientMessage->clientFd);
    free(params->clientMessage);
    free(params);
}

void readTerminal(WINDOW* transmission, struct clientMessage* clientMessage, struct sockaddr* serverAddr) {
    wgetstr(transmission, clientMessage->message);
    wmove(transmission, CURS_Y, CURS_X);
    wclrtoeol(transmission);
    wrefresh(transmission);

    if (clientMessage->message[0] != 0) {
        if (connect(clientMessage->clientFd, serverAddr, sizeof(*serverAddr)) == -1) {
            printf("Erreur lors de la connection du socket.");
            free(clientMessage);
            exit(EXIT_FAILURE);
        }
        send(clientMessage->clientFd, clientMessage->message, strlen(clientMessage->message), 0);
    }
}