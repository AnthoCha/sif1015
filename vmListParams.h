#include "vmList.h"

struct addVmParams {
    struct clientMessage* clientMessage;
    struct vmList* vmList;
};

struct removeVmAtParams {
    struct clientMessage* clientMessage;
    struct vmList* vmList;
    unsigned int index;
};

struct printVmListParams {
    struct clientMessage* clientMessage;
    struct vmList* vmList;
    unsigned int start;
    unsigned int end;
};

struct executeVmAtParams {
    struct clientMessage* clientMessage;
    struct vmList* vmList;
    unsigned int index;
    char* olc3;
};

void addVmWithParams(struct addVmParams* params);
void removeVmAtWithParams(struct removeVmAtParams* params);
void printVmListWithParams(struct printVmListParams* params);
int executeVmAtWithParams(struct executeVmAtParams* params);