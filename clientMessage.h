#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>

#define SERVER_FIFO "/tmp/trans.fifo"
#define CLIENT_FIFO_FORMAT "/tmp/%d.fifo"
#define CLIENT_MESSAGE_BUFFER 255
#define CLIENT_FIFO_BUFFER 127
#define MSG_ACK "ACK"
#define PORT 8687

struct clientMessage {
    int clientFd;
    char message[CLIENT_MESSAGE_BUFFER];
};

void writeToClient(struct clientMessage* clientMessage);
void writeAckToClient(struct clientMessage* clientMessage);