#include <stdlib.h>

#include "vmListParams.h"

void addVmWithParams(struct addVmParams* params) {
    struct clientMessage* clientMessage = params->clientMessage;
    struct vmList* vmList = params->vmList;
    free(params);

    addVm(vmList);
    writeAckToClient(clientMessage);
    free(clientMessage);
}

void removeVmAtWithParams(struct removeVmAtParams* params) {
    struct clientMessage* clientMessage = params->clientMessage;
    struct vmList* vmList = params->vmList;
    unsigned int index = params->index;
    free(params);

    removeVmAt(vmList, index);
    writeAckToClient(clientMessage);
    free(clientMessage);
}

void printVmListWithParams(struct printVmListParams* params) {
    struct clientMessage* clientMessage = params->clientMessage;
    struct vmList* vmList = params->vmList;
    unsigned int start = params->start;
    unsigned int end = params->end;
    free(params);

    printVmList(vmList, start, end, clientMessage);
    writeAckToClient(clientMessage);
    free(clientMessage);
}

int executeVmAtWithParams(struct executeVmAtParams* params) {
    struct clientMessage* clientMessage = params->clientMessage;
    struct vmList* vmList = params->vmList;
    unsigned int index = params->index;
    char* olc3 = params->olc3;
    free(params);

    int result = executeVmAt(vmList, index, olc3, clientMessage);
    writeAckToClient(clientMessage);
    free(clientMessage);
    return result;
}