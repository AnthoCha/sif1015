#include <stdlib.h>
#include "trans.h"

int main(int argc, char* argv[]) {
    struct vmList* vmList = newVmList();
    
    readTrans(vmList);

    freeVmList(vmList);  

    return 0;
}