#include <pthread.h>

#include "vmListParams.h"

void* readTrans(struct vmList* vmList);
void freeTransThread(pthread_t* tid);