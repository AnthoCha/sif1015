#include "linkedList.h"
#include "olc3.h"

struct vmList {
    struct linkedList* innerList;
};

struct vmList* newVmList();
void freeVmList(struct vmList* vmList);

void addVm(struct vmList* vmList);
void removeVmAt(struct vmList* vmList, const unsigned int index);
void printVmList(const struct vmList* vmList, const unsigned int start, const unsigned int end, struct clientMessage* clientMessage);
int executeVmAt(const struct vmList* vmList, const unsigned int index, char* olc3, struct clientMessage* clientMessage);