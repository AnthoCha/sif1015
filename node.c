#include <stdlib.h>
#include "node.h"

struct node* newNode(void* data, void (*freeDataCallback)(void*)) {
    struct node* node = malloc(sizeof(struct node));
    node->data = data;
    node->next = NULL;
    node->freeDataCallback = freeDataCallback;

    node->mutex = malloc(sizeof(*(node->mutex)));
    pthread_mutex_init(node->mutex, NULL);

    return node;
}

void freeNode(struct node* node) {
    if (node->freeDataCallback != NULL) {
        node->freeDataCallback(node->data);
    }

    pthread_mutex_destroy(node->mutex);
    free(node->mutex);
    free(node);
}

struct node* getNextNode(struct node* node) {
    struct node* next = node->next;

    pthread_mutex_unlock(node->mutex);

    if (next != NULL) {
        pthread_mutex_lock(next->mutex);
    }

    return next;
}