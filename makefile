all: main client

main: obj/main.o obj/clientMessage.o obj/trans.o obj/vmListParams.o obj/vmList.o obj/olc3.o obj/vm.o obj/linkedList.o obj/node.o
	mkdir -p bin
	gcc -pthread -o bin/main.exe $^

client: obj/client.o
	mkdir -p bin
	gcc -pthread -o bin/client.exe $^ -lncurses

obj/main.o: main.c trans.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/client.o: client.c clientMessage.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/clientMessage.o: clientMessage.c clientMessage.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/trans.o: trans.c trans.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/vmListParams.o: vmListParams.c vmListParams.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/vmList.o: vmList.c vmList.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/olc3.o: olc3.c olc3.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/vm.o: vm.c vm.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/linkedList.o: linkedList.c linkedList.h
	mkdir -p obj
	gcc -o $@ -g -c $<

obj/node.o: node.c node.h
	mkdir -p obj
	gcc -o $@ -g -c $<

clean:
	rm -rf bin
	rm -rf obj