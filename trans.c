#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/time.h>
#include <sys/types.h>

#include "trans.h"

void* readTrans(struct vmList* vmList) {
    char quit = 0;
    char* buffer;
    char* tok, * sp;

    int listenFd = socket(AF_INET, SOCK_STREAM, 0);

    if (listenFd == -1) {
        perror("Erreur lors de la création du socket.");
        exit(EXIT_FAILURE);
    }

    int opt = 1;
    if (setsockopt(listenFd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)) == -1) {
        perror("Erreur lors de la définition des options du socket.");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in clientAddr;
    clientAddr.sin_family = AF_INET;
    clientAddr.sin_addr.s_addr = INADDR_ANY;
    clientAddr.sin_port = htons(PORT);

    int addrLength = sizeof(clientAddr);
    if (bind(listenFd, &clientAddr, addrLength) == -1) {
        perror("Erreur lors de la liaison au socket.");
        exit(EXIT_FAILURE);
    }

    if (listen(listenFd, 0) == -1) {
        perror("Erreur lors de la lecture du socket.");
        exit(EXIT_FAILURE);
    }

    struct linkedList* threadList = newLinkedList();

    struct clientMessage* clientMessage = malloc(sizeof(struct clientMessage));

    while (quit == 0) {
        clientMessage->clientFd = accept(listenFd, &clientAddr, &addrLength);
        if (clientMessage->clientFd != -1 &&
            read(clientMessage->clientFd, clientMessage->message, sizeof(clientMessage->message)) != -1 &&
            clientMessage->message[0] != 0) {
            buffer = clientMessage->message;

            //Extraction du type de transaction
            tok = strtok_r(buffer, " ", &sp);

            //Branchement selon le type de transaction
            switch (tok[0]) {
                case 'A':
                case 'a': {
                    //Appel de la fonction associée
                    struct addVmParams* params = malloc(sizeof(struct addVmParams));
                    params->clientMessage = clientMessage;
                    params->vmList = vmList;

                    pthread_t* tid = malloc(sizeof(pthread_t));

                    if (pthread_create(tid, NULL, addVmWithParams, params) != 0) {
                        perror("readTrans: Erreur lors de la création d'un thread.");
                        exit(4);
                    } // Ajout de une VM

                    add(threadList, tid, freeTransThread);
                    break;
                }
                case 'E':
                case 'e': {
                    //Extraction du paramètre
                    int index = atoi(strtok_r(NULL, " ", &sp)) - 1;

                    //Appel de la fonction associée
                    struct removeVmAtParams* params = malloc(sizeof(struct removeVmAtParams));
                    params->clientMessage = clientMessage;
                    params->vmList = vmList;
                    params->index = index;

                    pthread_t* tid = malloc(sizeof(pthread_t));

                    if (pthread_create(tid, NULL, removeVmAtWithParams, params) != 0) {
                        perror("readTrans: Erreur lors de la création d'un thread.");
                        exit(4);
                    } // Eliminer une VM

                    add(threadList, tid, freeTransThread);
                    break;
                }
                case 'L':
                case 'l': {
                    //Extraction des paramètres
                    int start = atoi(strtok_r(NULL, "-", &sp)) - 1;
                    int end = atoi(strtok_r(NULL, " ", &sp));
                    //Appel de la fonction associée
                    struct printVmListParams* params = malloc(sizeof(struct printVmListParams));
                    params->clientMessage = clientMessage;
                    params->vmList = vmList;
                    params->start = start;
                    params->end = end;

                    pthread_t* tid = malloc(sizeof(pthread_t));

                    if (pthread_create(tid, NULL, printVmListWithParams, params) != 0) {
                        perror("readTrans: Erreur lors de la création d'un thread.");
                        exit(4);
                    } // Lister les VM

                    add(threadList, tid, freeTransThread);
                    break;
                }
                case 'X':
                case 'x': {
                    //Appel de la fonction associée
                    int index = atoi(strtok_r(NULL, " ", &sp));
                    char* olc3 = strtok_r(NULL, "\n", &sp);
                    olc3 = strtok_r(NULL, "\r", &olc3);

                    struct executeVmAtParams* params = malloc(sizeof(struct executeVmAtParams));
                    params->clientMessage = clientMessage;
                    params->vmList = vmList;
                    params->index = index;
                    params->olc3 = olc3;

                    pthread_t* tid = malloc(sizeof(pthread_t));

                    if (pthread_create(tid, NULL, executeVmAtWithParams, params) != 0) {
                        perror("readTrans: Erreur lors de la création d'un thread.");
                        exit(4);
                    } // Executer le code binaire du fichier nomFich sur la VM noVM

                    add(threadList, tid, freeTransThread);
                    break;
                }
                default: {
                    strcpy(clientMessage->message, "Erreur lors de la la lecture de la transaction.");
                    writeToClient(clientMessage);
                    writeAckToClient(clientMessage);
                    free(clientMessage);
                    break;
                }
            }

            clientMessage = malloc(sizeof(struct clientMessage));
        }
    }

    // Attendre la terminaison des threads
    freeLinkedList(threadList);

    free(clientMessage);

    // Fermeture du socket
    close(listenFd);

    //Retour
    return NULL;
}

void freeTransThread(pthread_t* tid) {
    pthread_join(*tid, NULL);
    free(tid);
}