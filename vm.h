struct vm {
    unsigned int id;
    char busy;
    unsigned short* ptr;
};

struct vm* newVm();
void freeVm(struct vm* vm);
