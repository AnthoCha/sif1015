#include "clientMessage.h"

void writeToClient(struct clientMessage* clientMessage) {
    if (clientMessage->clientFd != -1) {
        send(clientMessage->clientFd, clientMessage->message, CLIENT_MESSAGE_BUFFER, 0);
    }
}


void writeAckToClient(struct clientMessage* clientMessage) {
    strcpy(clientMessage->message, MSG_ACK);
    writeToClient(clientMessage);
    close(clientMessage->clientFd);
}