#include <stdlib.h>
#include "linkedList.h"

struct linkedList* newLinkedList() {
    struct linkedList* linkedList = malloc(sizeof(struct linkedList));
    linkedList->first = NULL;
    linkedList->last = NULL;
    linkedList->count = 0;

    linkedList->firstMutex = malloc(sizeof(*(linkedList->firstMutex)));
    linkedList->lastMutex = malloc(sizeof(*(linkedList->lastMutex)));
    linkedList->countMutex = malloc(sizeof(*(linkedList->countMutex)));

    pthread_mutex_init(linkedList->firstMutex, NULL);
    pthread_mutex_init(linkedList->lastMutex, NULL);
    pthread_mutex_init(linkedList->countMutex, NULL);

    return linkedList;
}

void freeLinkedList(struct linkedList* linkedList) {
    pthread_mutex_lock(linkedList->firstMutex);
    struct node* node = linkedList->first;
    struct node* next = NULL;

    if (node != NULL) {
        pthread_mutex_lock(node->mutex);
    }

    pthread_mutex_unlock(linkedList->firstMutex);

    while (node != NULL) {
        next = node->next;

        if (next != NULL) {
            pthread_mutex_lock(next->mutex);
        }

        freeNode(node);
        node = next;
    }

    pthread_mutex_destroy(linkedList->firstMutex);
    free(linkedList->firstMutex);

    pthread_mutex_destroy(linkedList->lastMutex);
    free(linkedList->lastMutex);

    pthread_mutex_destroy(linkedList->countMutex);
    free(linkedList->countMutex);

    free(linkedList);
}

struct node* getNodeAt(struct linkedList* linkedList, const unsigned int index) {
    unsigned int i = 0;
    pthread_mutex_lock(linkedList->firstMutex);
    struct node* node = linkedList->first;


    if (node != NULL) {
        pthread_mutex_lock(node->mutex);
    }

    pthread_mutex_unlock(linkedList->firstMutex);

    while (node != NULL && i < index) {
        i++;

        node = getNextNode(node);
    }

    return node;
}

void* getAt(struct linkedList* linkedList, const unsigned int index) {
    struct node* node = getNodeAt(linkedList, index);

    if (node == NULL) {
        return NULL;
    } else {
        pthread_mutex_unlock(node->mutex);
    }
    return node->data;
}

int addNode(struct linkedList* linkedList, struct node* node) {
    pthread_mutex_lock(linkedList->firstMutex);

    if (linkedList->first == NULL) {
        linkedList->first = node;

        pthread_mutex_unlock(linkedList->firstMutex);
        pthread_mutex_lock(linkedList->lastMutex);

        linkedList->last = node;
    } else {
        pthread_mutex_unlock(linkedList->firstMutex);
        pthread_mutex_lock(linkedList->lastMutex);
        pthread_mutex_lock(linkedList->last->mutex);

        linkedList->last->next = node;

        pthread_mutex_unlock(linkedList->last->mutex);

        linkedList->last = node;
    }

    pthread_mutex_unlock(linkedList->lastMutex);

    pthread_mutex_lock(linkedList->countMutex);
    unsigned int index = linkedList->count++;
    pthread_mutex_unlock(linkedList->countMutex);
    return index;
}

int add(struct linkedList* linkedList, void* data, void (*freeDataCallback)(void*)) {
    struct node* node = newNode(data, freeDataCallback);
    return addNode(linkedList, node);
}

void removeAt(struct linkedList* linkedList, const unsigned int index) {
    struct node* node = NULL;
    struct node* prev = NULL;

    if (index == 0) {
        pthread_mutex_lock(linkedList->firstMutex);
        node = linkedList->first;

        pthread_mutex_lock(node->mutex);
        linkedList->first = node->next;
        pthread_mutex_unlock(node->mutex);

        pthread_mutex_unlock(linkedList->firstMutex);

    } else {
        prev = getNodeAt(linkedList, index - 1);

        if (prev != NULL) {
            node = prev->next;
        }
    }

    if (node != NULL) {
        pthread_mutex_lock(linkedList->lastMutex);

        if (node == linkedList->last) {
            linkedList->last = prev;
        }

        pthread_mutex_unlock(linkedList->lastMutex);

        if (prev != NULL) {
            pthread_mutex_lock(node->mutex);
            prev->next = node->next;

            pthread_mutex_unlock(prev->mutex);
        }

        freeNode(node);

        pthread_mutex_lock(linkedList->countMutex);
        linkedList->count--;
        pthread_mutex_unlock(linkedList->countMutex);
    } else if (prev != NULL) {
        pthread_mutex_unlock(prev->mutex);
    }
}