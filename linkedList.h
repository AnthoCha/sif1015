#include <pthread.h>

#include "node.h"

struct linkedList {
    struct node* first;
    struct node* last;
    unsigned int count;
    pthread_mutex_t* firstMutex;
    pthread_mutex_t* lastMutex;
    pthread_mutex_t* countMutex;
};

struct linkedList* newLinkedList();
void freeLinkedList(struct linkedList* linkedList);

void* getAt(struct linkedList* linkedList, const unsigned int index);
struct node* getNodeAt(struct linkedList* linkedList, const unsigned int index);
int add(struct linkedList* linkedList, void* data, void (*freeDataCallback)(void*));
int addNode(struct linkedList* linkedList, struct node* node);
void removeAt(struct linkedList* linkedList, const unsigned int index);