#include <pthread.h>

struct node {
    void* data;
    struct node* next;
    void (*freeDataCallback)(void*);
    pthread_mutex_t* mutex;
};

struct node* newNode(void* data, void (*freeDataCallback)(void*));
void freeNode(struct node* node);

struct node* getNextNode(struct node* node);