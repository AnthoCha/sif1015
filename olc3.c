#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/termios.h>
#include <sys/mman.h>

#include "olc3.h"

/* Input Buffering */
struct termios original_tio;

unsigned short sign_extend(unsigned short x, int bit_count) {
    if ((x >> (bit_count - 1)) & 1) {
        x |= (0xFFFF << bit_count);
    }
    return x;
}

/* Swap */
unsigned short swap16(unsigned short x) {
    return (x << 8) | (x >> 8);
}

/* Update Flags */
void update_flags(unsigned short reg[R_COUNT], unsigned short r) {
    if (reg[r] == 0) {
        reg[R_COND] = FL_ZRO;
    } else if (reg[r] >> 15) /* a 1 in the left-most bit indicates negative */
    {
        reg[R_COND] = FL_NEG;
    } else {
        reg[R_COND] = FL_POS;
    }
}

/* Read Image File */
int read_image_file(unsigned short* memory, char* image_path, unsigned short* origin) {
    char fich[200];
    strcpy(fich, image_path);
    FILE* file = fopen(fich, "rb");

    if (!file) { return 0; }
    /* the origin tells us where in memory to place the image */
    *origin = 0x3000;

    /* we know the maximum file size so we only need one fread */
    unsigned short max_read = UINT16_MAX - *origin;
    unsigned short* p = memory + *origin;
    size_t read = fread(p, sizeof(unsigned short), max_read, file);
    /* swap to little endian */
    while (read-- > 0) {
        ++p;
    }
    return 1;
}

/* Check Key */
unsigned short check_key() {
    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(STDIN_FILENO, &readfds);

    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;
    return select(1, &readfds, NULL, NULL, &timeout) != 0;
}

/* Memory Access */
void mem_write(unsigned short* memory, unsigned short address, unsigned short val) {
    memory[address] = val;
}

unsigned short mem_read(unsigned short* memory, unsigned short address) {
    if (address == MR_KBSR) {
        if (check_key()) {
            memory[MR_KBSR] = (1 << 15);
            memory[MR_KBDR] = getchar();
        } else {
            memory[MR_KBSR] = 0;
        }
    }
    return memory[address];
}

void disable_input_buffering() {
    tcgetattr(STDIN_FILENO, &original_tio);
    struct termios new_tio = original_tio;
    new_tio.c_lflag &= ~ICANON & ~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &new_tio);
}

void restore_input_buffering() {
    tcsetattr(STDIN_FILENO, TCSANOW, &original_tio);
}

/* Handle Interrupt */
void handle_interrupt(int signal) {
    restore_input_buffering();
    exit(-2);
}

int executeVm(struct vm* vm, char* olc3, struct clientMessage* clientMessage) {
    /* Memory Storage */
    /* 65536 locations */
    unsigned short* memory;
    unsigned short origin;
    unsigned short PC_START;

    /* Register Storage */
    unsigned short reg[R_COUNT];

    if (vm == NULL) {
        strcpy(clientMessage->message, "Virtual Machine unavailable");
        writeToClient(clientMessage);
        return 0;
    }

    memory = vm->ptr;
    if (!read_image_file(memory, olc3, &origin)) {
        sprintf(clientMessage->message, "Failed to load image: %s", olc3);
        writeToClient(clientMessage);
        return 0;
    }

    while (vm->busy != 0) { // wait for the VM 
    }
    // Acquiring access to the VM
    vm->busy = 1;

    /* Setup */
    signal(SIGINT, handle_interrupt);
    disable_input_buffering();

    /* set the PC to starting position */
    /* 0x3000 is the default  */
    PC_START = origin;
    reg[R_PC] = PC_START;

    int running = 1;
    while (running) {
        /* FETCH */
        unsigned short instr = mem_read(memory, reg[R_PC]++);
        unsigned short op = instr >> 12;

        switch (op) {
            case OP_ADD:
                /* ADD */
            {
                /* destination register (DR) */
                unsigned short r0 = (instr >> 9) & 0x7;
                /* first operand (SR1) */
                unsigned short r1 = (instr >> 6) & 0x7;
                /* whether we are in immediate mode */
                unsigned short imm_flag = (instr >> 5) & 0x1;

                if (imm_flag) {
                    unsigned short imm5 = sign_extend(instr & 0x1F, 5);
                    reg[r0] = reg[r1] + imm5;
                } else {
                    unsigned short r2 = instr & 0x7;
                    reg[r0] = reg[r1] + reg[r2];
                    sprintf(clientMessage->message, "add reg[r0] (sum) = %d", reg[r0]);
                    writeToClient(clientMessage);
                }

                update_flags(reg, r0);
            }

            break;
            case OP_AND:
                /* AND */
            {
                unsigned short r0 = (instr >> 9) & 0x7;
                unsigned short r1 = (instr >> 6) & 0x7;
                unsigned short imm_flag = (instr >> 5) & 0x1;

                if (imm_flag) {
                    unsigned short imm5 = sign_extend(instr & 0x1F, 5);
                    reg[r0] = reg[r1] & imm5;
                } else {
                    unsigned short r2 = instr & 0x7;
                    reg[r0] = reg[r1] & reg[r2];
                }
                update_flags(reg, r0);
            }

            break;
            case OP_NOT:
                /* NOT */
            {
                unsigned short r0 = (instr >> 9) & 0x7;
                unsigned short r1 = (instr >> 6) & 0x7;

                reg[r0] = ~reg[r1];
                update_flags(reg, r0);
            }

            break;
            case OP_BR:
                /* BR */
            {
                unsigned short pc_offset = sign_extend(instr & 0x1FF, 9);
                unsigned short cond_flag = (instr >> 9) & 0x7;
                if (cond_flag & reg[R_COND]) {
                    reg[R_PC] += pc_offset;
                }
            }

            break;
            case OP_JMP:
                /* JMP */
            {
                /* Also handles RET */
                unsigned short r1 = (instr >> 6) & 0x7;
                reg[R_PC] = reg[r1];
            }

            break;
            case OP_JSR:
                /* JSR */
            {
                unsigned short long_flag = (instr >> 11) & 1;
                reg[R_R7] = reg[R_PC];
                if (long_flag) {
                    unsigned short long_pc_offset = sign_extend(instr & 0x7FF, 11);
                    reg[R_PC] += long_pc_offset;  /* JSR */
                } else {
                    unsigned short r1 = (instr >> 6) & 0x7;
                    reg[R_PC] = reg[r1]; /* JSRR */
                }
                break;
            }

            break;
            case OP_LD:
                /* LD */
            {
                unsigned short r0 = (instr >> 9) & 0x7;
                unsigned short pc_offset = sign_extend(instr & 0x1FF, 9);
                reg[r0] = mem_read(memory, reg[R_PC] + pc_offset);
                update_flags(reg, r0);
            }

            break;
            case OP_LDI:
                /* LDI */
            {
                /* destination register (DR) */
                unsigned short r0 = (instr >> 9) & 0x7;
                /* PCoffset 9*/
                unsigned short pc_offset = sign_extend(instr & 0x1FF, 9);
                /* add pc_offset to the current PC, look at that memory location to get the final address */
                reg[r0] = mem_read(memory, mem_read(memory, reg[R_PC] + pc_offset));
                update_flags(reg, r0);
            }

            break;
            case OP_LDR:
                /* LDR */
            {
                unsigned short r0 = (instr >> 9) & 0x7;
                unsigned short r1 = (instr >> 6) & 0x7;
                unsigned short offset = sign_extend(instr & 0x3F, 6);
                reg[r0] = mem_read(memory, reg[r1] + offset);
                update_flags(reg, r0);
            }

            break;
            case OP_LEA:
                /* LEA */
            {
                unsigned short r0 = (instr >> 9) & 0x7;
                unsigned short pc_offset = sign_extend(instr & 0x1FF, 9);
                reg[r0] = reg[R_PC] + pc_offset;
                update_flags(reg, r0);
            }

            break;
            case OP_ST:
                /* ST */
            {
                unsigned short r0 = (instr >> 9) & 0x7;
                unsigned short pc_offset = sign_extend(instr & 0x1FF, 9);
                mem_write(memory, reg[R_PC] + pc_offset, reg[r0]);
            }

            break;
            case OP_STI:
                /* STI */
            {
                unsigned short r0 = (instr >> 9) & 0x7;
                unsigned short pc_offset = sign_extend(instr & 0x1FF, 9);
                mem_write(memory, mem_read(memory, reg[R_PC] + pc_offset), reg[r0]);
            }

            break;
            case OP_STR:
                /* STR */
            {
                unsigned short r0 = (instr >> 9) & 0x7;
                unsigned short r1 = (instr >> 6) & 0x7;
                unsigned short offset = sign_extend(instr & 0x3F, 6);
                mem_write(memory, reg[r1] + offset, reg[r0]);
            }

            break;
            case OP_TRAP:
                /* TRAP */
                switch (instr & 0xFF) {
                    case TRAP_GETC:
                        /* TRAP GETC */
                        /* read a single ASCII char */
                        reg[R_R0] = (unsigned short)getchar();

                        break;
                    case TRAP_OUT:
                        /* TRAP OUT */
                        putc((char)reg[R_R0], stdout);
                        fflush(stdout);

                        break;
                    case TRAP_PUTS:
                        /* TRAP PUTS */
                    {
                        /* one char per word */
                        unsigned short* c = memory + reg[R_R0];
                        while (*c) {
                            putc((char)*c, stdout);
                            ++c;
                        }
                        fflush(stdout);
                    }

                    break;
                    case TRAP_IN:
                        /* TRAP IN */
                    {
                        strcpy(clientMessage->message, "Enter a character: ");
                        writeToClient(clientMessage);
                        char c = getchar();
                        putc(c, stdout);
                        reg[R_R0] = (unsigned short)c;
                    }

                    break;
                    case TRAP_PUTSP:
                        /* TRAP PUTSP */
                    {
                        /* one char per byte (two bytes per word)
                           here we need to swap back to
                           big endian format */
                        unsigned short* c = memory + reg[R_R0];
                        while (*c) {
                            char char1 = (*c) & 0xFF;
                            putc(char1, stdout);
                            char char2 = (*c) >> 8;
                            if (char2) putc(char2, stdout);
                            ++c;
                        }
                        fflush(stdout);
                    }

                    break;
                    case TRAP_HALT:
                        /* TRAP HALT */
                        strcpy(clientMessage->message, "HALT");
                        writeToClient(clientMessage);
                        fflush(stdout);
                        running = 0;

                        break;
                }

                break;
            case OP_RES:
            case OP_RTI:
            default:
                /* BAD OPCODE */
                abort();

                break;
        }
    }
    vm->busy = 0;

    /* Shutdown */
    restore_input_buffering();
    return 1;
}

