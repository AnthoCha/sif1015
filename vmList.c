#include <stdlib.h>
#include <stdio.h>

#include "vmList.h"

struct vmList* newVmList() {
    struct vmList* vmList = malloc(sizeof(struct vmList));
    vmList->innerList = newLinkedList();
    return vmList;
}

void freeVmList(struct vmList* vmList) {
    freeLinkedList(vmList->innerList);
    free(vmList);
}

void addVm(struct vmList* vmList) {
    struct vm* vm = newVm();
    unsigned int index = add(vmList->innerList, vm, freeVm);
    vm->id = index + 1;
}

void removeVmAt(struct vmList* vmList, const unsigned int index) {
    struct node* node = getNodeAt(vmList->innerList, index);

    if (node != NULL) {
        while (node != NULL) {
            struct vm* vm = (struct vm*)node->data;
            vm->id--;

            node = getNextNode(node);
        }

        removeAt(vmList->innerList, index);
    }
}

void printVmList(const struct vmList* vmList, const unsigned int start, const unsigned int end, struct clientMessage* clientMessage) {
    struct node* node = getNodeAt(vmList->innerList, start);
    unsigned int index = start;

    sprintf(clientMessage->message, "%6s %20s %33s", "Id", "En cours d'exécution", "Adresse mémoire du début de la VM");
    writeToClient(clientMessage);

    while (node != NULL && index < end) {
        struct vm* vm = (struct vm*)node->data;
        sprintf(clientMessage->message, "%6d %20d %33p", vm->id, vm->busy, vm->ptr);
        writeToClient(clientMessage);

        node = getNextNode(node);
        index++;
    }

    if (node != NULL) {
        pthread_mutex_unlock(node->mutex);
    }
}

int executeVmAt(const struct vmList* vmList, const unsigned int index, char* olc3, struct clientMessage* clientMessage) {
    return executeVm(getAt(vmList->innerList, index), olc3, clientMessage);
}