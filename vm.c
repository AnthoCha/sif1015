#include <stdlib.h>
#include "vm.h"

struct vm* newVm(const unsigned int id) {
    struct vm* vm = malloc(sizeof(struct vm));
    vm->id = 0;
    vm->busy = 0;
    vm->ptr = calloc(65536, sizeof(unsigned short));
    return vm;
}

void freeVm(struct vm* vm) {
    free(vm->ptr);
    free(vm);
}